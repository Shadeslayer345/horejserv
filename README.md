# Horjserv
#### (Ho)t (R)eloading (j̶)s̶p̶m (Javascript) (ser)ver

Playing around with hot-reloading with j̶s̶p̶m.

This has become a starting point for basing my future endeavours w/ React on.

TLDR; If you wanna know why I use certain technologies read [here](why.md)

Stack:

* App
    - [jade](http://jade-lang.com/) now pug
    - [nodejs](https://nodejs.org/)
    - [nodemon](http://nodemon.io/)
    - [react](https://facebook.github.io/react/)
    - [react-pacomo](http://unicornstandard.com/packages/react-pacomo.html)
    - [stylus](http://stylus-lang.com/)
    - Data/State Handling
        + [flux](https://facebook.github.io/flux/)
        + [Alt](http://altjs.org)
        + [mobservable]()
* Dev work
    - Transpiling (es6+ -> es5)
        + [babel](https://bablejs.io)
        + [babel-preset-es2015](https://babeljs.io/docs/plugins/preset-es2015)
        + [babel-preset-react](https://babeljs.io/docs/plugins/preset-react)
        + [babel-preset-stage-0](https://babeljs.io/docs/plugins/preset-stage-0)
        + [babel-register](https://babeljs.io/docs/usage/require)
        + [babelify](https://github.com/babel/babelify)
        + [transform-decorators](https://www.npmjs.com/package/babel-plugin-transform-decorators)
        + [browserify](http://browserify.org)
    - Linting
        + [babel-eslint](https://github.com/babel/babel-eslint)
        + [eslint](http://eslint.org/)
            * [eslint-plugin-react](https://github.com/yannickcr/eslint-plugin-react)
        + [stylint](https://www.npmjs.com/package/stylint)
    - Live-reloading
        + [babel-plugin-react-transform](https://github.com/gaearon/babel-plugin-react-transform)
        + [livereactload](https://github.com/milankinen/livereactload)
        + [react-proxy](https://github.com/gaearon/react-proxy)
        + [watchify](https://github.com/substack/watchify)
        + [parallel](https://www.npmjs.com/package/parallel)
        + [parallelshell](https://www.npmjs.com/package/parallelshell)
    - [devtools-terminal](http://blog.dfilimonov.com/2013/09/12/devtools-terminal.html)
* Testing
    - Mocking
        + [proxyquire](https://www.npmjs.com/package/proxyquire)
    - React
        + [react-addons-test-utils](https://facebook.github.io/react/docs/test-utils.html)
        + [react-unit](https://www.npmjs.com/package/react-unit)
    - Client-side Testing
        + [devtool](https://www.npmjs.com/package/devtool)
    - Stubbing and Spying
        + [sinon](http://sinonjs.org)
    - TAP
        + [tap-closer](https://www.npmjs.com/package/tap-closer)
        + [tap-spec](https://www.npmjs.com/package/tap-spec)
        + [tape](https://www.npmjs.com/package/tape)
        + [tape-jsx-equals](https://www.npmjs.com/package/tape-jsx-equals)

:sunglasses:

* Coming Soon:
    - Routing
        + [alt-router]()
    - Immutable Data
        + [immutable-seamless](https://github.com/rtfeldman/seamless-immutable)
        + [immutable.js](https://facebook.github.io/immutable-js/)
        + [mori](http://swannodette.github.io/mori)