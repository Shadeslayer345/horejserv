import addAssertions from 'extend-tape';
import createComponent from 'react-unit';
import {createRenderer} from 'react-addons-test-utils';
import jsxEquals from 'tape-jsx-equals';
import React from 'react';
import tape from 'tape';

import Output from '../../app/src/js/components/output.jsx';

const test = addAssertions(tape, {jsxEquals});

test(`----- Horejserv React Component Test: Output -----`, (swear) => {
  const
    swearJar = 5,
    imagineComponentClassName = `horejserv-Output `,
    imagineH3ClassName = ` horejserv-Output-content `,
    imagineH3Text = `value`,
    bonafideComponent = createComponent.shallow(<Output value={`test`} />),
    bonafideH3 = bonafideComponent.findByQuery(`h3`)[0],
    renderer = createRenderer();

  swear.plan(swearJar);
  swear.comment(`Output component should have correct props`);
  swear.equal(bonafideComponent.props.className, imagineComponentClassName,
      `renders w/ correct prop -- className`);
  swear.equal(bonafideH3.text, imagineH3Text,
      `renders w/ correct child prop -- text`);
  swear.equal(bonafideH3.props.className, imagineH3ClassName,
      `renders w/ correct child prop -- className`);
  swear.comment(`Output component should render correctly`);
  renderer.render(<Output value={`test`} />);
  swear.jsxEquals(renderer.getRenderOutput(),
      (<div className={`horejserv-Output `}>
        <h3 className={` horejserv-Output-content `}>{`test`}</h3>
      </div>), `renders jsx correctly`);
});
