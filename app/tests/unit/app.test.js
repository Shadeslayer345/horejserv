import addAssertions from 'extend-tape';
import createComponent from 'react-unit';
import {createRenderer} from 'react-addons-test-utils';
import jsxEquals from 'tape-jsx-equals';
import proxyquire from 'proxyquire';
import React from 'react';
import sinon from 'sinon';
import tape from 'tape';

import App from '../../app/src/js/components/App.jsx';

const test = addAssertions(tape, {jsxEquals});
