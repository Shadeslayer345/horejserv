import addAssertions from 'extend-tape';
import createComponent from 'react-unit';
import {createRenderer} from 'react-addons-test-utils';
import jsxEquals from 'tape-jsx-equals';
import React from 'react';
import sinon from 'sinon';
import tape from 'tape';

import Input from '../../app/src/js/components/input.jsx';

const test = addAssertions(tape, {jsxEquals});

test(`----- Horejserv React Component Test: Input -----`, (swear) => {
  const
    swearJar = 8,
    imagineCallbackStub = sinon.stub(),
    imagineCallbackCount = 1,
    imagineComponentClassName = `horejserv-Input `,
    imagineTextAreaClassName = ` horejserv-Input-content `,
    imagineTextAreaPlaceHolder = `Hello Testolo`,
    bonafideComponent = createComponent(
      <Input
          onChangeHandler={imagineCallbackStub}
          placeHolder={`Hello Testolo`}
      />
    ),
    bonafideTextArea = bonafideComponent.findByQuery(`textarea`)[0],
    renderer = createRenderer();

  swear.plan(swearJar);
  swear.comment(`Input component should have correct props`);
  swear.equal(bonafideComponent.props.className, imagineComponentClassName,
      `renders w/ correct prop -- className`);
  swear.equal(bonafideTextArea.props.onChange, imagineCallbackStub,
      `renders w/ correct prop -- onChange`);
  swear.equal(bonafideTextArea.props.placeHolder, imagineTextAreaPlaceHolder,
      `renders w/ correct prop -- placeHolder`);
  swear.equal(bonafideTextArea.props.className, imagineTextAreaClassName,
      `renders w/ correct child -- prop -- className`);
  swear.comment(`Input component should behave correctly`);
  bonafideTextArea.props.onChange({target: {value: `Update`}}); // Let's simulate this instead, yea?
  swear.equal(imagineCallbackStub.callCount, imagineCallbackCount,
      `behaves w/ correct callback count -- 1`);
  swear.ok(imagineCallbackStub.calledWith({target: {value: `Update`}}),
      `behaves w/ callback in pure fashion`);
  swear.comment(`Input component should render correct jsx`);
  renderer.render(
      <Input
          onChangeHandler={imagineCallbackStub}
          placeHolder={`Hello Testolo`}
      />);
  swear.jsxEquals(renderer.getRenderOutput(),
      (<div className={`horejserv-Input `}>
        <textarea
            className={` horejserv-Input-content `}
            onChange={imagineCallbackStub}
            placeholder={`Hello Testolo`}
        />
      </div>),
      `renders correctly`);
});
