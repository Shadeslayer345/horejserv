import React, {PropTypes} from 'react';

import {pacomoTransformer} from '../utils/pacomo';
import Input from './input.jsx';
import Output from './output.jsx';

/**
 * Simple component to live update element display text based on input
 *
 * @class
 * @param      {Object{String, func, String}  }       props   Our props object
 *                                                    destructured, check
 *                                                    proptypes
 * @return     {React.Component}
 */
const App = ({placeHolderValue, updateOutput, value}) => {
  return (
    <div>
      <Input
          handleChange={updateOutput}
          placeHolder={placeHolderValue}
      />
      <Output value={value} />
    </div>
  );
};

App.displayName = `App`;

App.propTypes = {
  placeHolderValue: PropTypes.string.isRequired,
  updateOutput: PropTypes.func,
  value: PropTypes.string
};

export default pacomoTransformer(App);
