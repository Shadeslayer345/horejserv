import React, {PropTypes} from 'react';

import {pacomoTransformer} from '../utils/pacomo';

/**
 * Simple h3 title display component
 *
 * @class
 * @param      {Object{String}   Our props
 * @param      {string}  value   Text to display
 * @return     {React.Component}
 */
const Output = ({value}) => {
  return (
    <div>
      <h3 className={`content`}>{value}</h3>
    </div>
  );
};

Output.displayName = `Output`;

Output.propTypes = {
  value: PropTypes.string.isRequired
};

export default pacomoTransformer(Output);
