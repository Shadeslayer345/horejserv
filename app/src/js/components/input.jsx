import React, {PropTypes} from 'react';

import {pacomoTransformer} from '../utils/pacomo';

/**
 * Text input component that actions our stores on change
 *
 * @class
 * @param      {Object{func, String}  }  Our props destructured, check proptypes
 *                                       proptypes
 * @return     {React.component}
 */
const Input = ({handleChange, placeHolder}) => {
  return (
    <div>
      <textarea
          className={`content`}
          onChange={handleChange}
          placeholder={placeHolder}
      />
    </div>
  );
};

Input.displayName = `Input`;

Input.propTypes = {
  handleChange: PropTypes.func.isRequired,
  placeHolder: PropTypes.string.isRequired
};

export default pacomoTransformer(Input);
