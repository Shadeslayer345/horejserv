import HorejservFlux from '../flux';

import HorejservActions from '../actions/actions';

/**
 * Store for our flux implementation
 * @class
 */
class HorejservStore {

  /**
   * Create our state and bind listeners to actions
   *
   * @method     constructor
   * @return     {void}
   */
  constructor () {
    this.value = ``;

    this.bindAction(HorejservActions.UPDATE_OUTPUT, this.handleUpdateOutput);
  }

  /**
   * Take value returned from action and set it to our state (make immutable
   * soon)
   *
   * @param      {string}  value   Output returned from action
   * @return     {void}
   */
  handleUpdateOutput = (value) => {
    this.value = value;
  };

  /**
   * Return a representation of the current state contained in this store
   *
   * @return     {Object}  The current state
   */
  getState = () => {
    return {
      value: this.value
    };
  };
}

export default HorejservFlux.createStore(HorejservStore, `HorejservStore`);
