import HorejservFlux from '../flux';

/**
 * Actions for our flux implementation
 * @class
 */
export default class HorejservActions {
  /**
   * Update the value property of state
   *
   * @method     updateOutput
   * @param      {synEvent}  updateEvent  The synthetic event coming from the
   *                                      app
   * @return     {void}
   */
  updateOutput = (updateEvent) => {
    return updateEvent.target.value;
  };
}

export default HorejservFlux.createActions(HorejservActions);
