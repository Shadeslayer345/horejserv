import React from 'react';
import {render} from 'react-dom';
import AltContainer from 'alt-container';

import App from './components/app.jsx';
import HorejservActions from './flux/actions/actions';
import HorejservStore from './flux/stores/store';

render(
  <AltContainer
      actions={HorejservActions}
      store={HorejservStore}
  >
    <App placeHolderValue={`Hello Solo`} />
  </AltContainer>,
  document.getElementById(`horejserv-App-root`));
